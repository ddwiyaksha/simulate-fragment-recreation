package com.example.example

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.simulatefragmentrecreation.R
import io.reactivex.Single
import kotlinx.android.synthetic.main.fragment_simple.*
import java.lang.RuntimeException


class ChildFragment : Fragment() {

    var message: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_simple, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.e("something", "onView Created")
        super.onViewCreated(view, savedInstanceState)
        messageView.text = message
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.e("something", "onActivity Created")
        super.onActivityCreated(savedInstanceState)

        Single.just("something")
            .subscribe({
                throw RuntimeException("just testing dude")
            }, {
                Log.e("something", "error $it")
            })
    }
}