package com.example.example

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.simulatefragmentrecreation.R
import kotlinx.android.synthetic.main.fragment_simple.*


class SimpleFragment : Fragment() {

    lateinit var message: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_simple, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        messageView.text = "Halo now"

        var handler: Handler? = Handler()
        handler?.postDelayed({
            messageView.text = " Halo now after 5000 milisecond"
        }, 5000)
        handler = null
    }
}