package com.example.example

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.simulatefragmentrecreation.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SimpleFragment().apply {
                    this.message = "This will be crashed when recreated"
                }).commit()
        }
    }
}
