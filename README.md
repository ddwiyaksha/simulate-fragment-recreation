# Zilla Android Application (alias Croissant)

Don't miss **SECURITY.md** and **CHANGELOD.md** as well.

## Links

- [Trello](https://trello.com/b/NhsD2C6B/android)
- [API Documentation](https://staging.zla.io/en/admin/apidoc/app)
- [Zeplin](https://app.zeplin.io/project/5a8fc55b0d2697642e3bab0b)
- Staging [user login](http://staging.zla.io/en/user/signin) and [admin login](staging.zla.io/en/admin/dashboard).
- [bitbucket-pipelines.yml validator](https://bitbucket-pipelines.prod.public.atl-paas.net/validator)

## Onboarding
1. [Cheatsheet] [Tips and tricks for Android Development](https://github.com/nisrulz/android-tips-tricks)
2. [A successful XML naming convention](https://jeroenmols.com/blog/2016/03/07/resourcenaming/)
3. [Configuring Android Studio](https://medium.com/google-developer-experts/configuring-android-studio-4aa4f54f1153)

## Build

Build debug build:

```
./gradlew :app:assembleDebug
```

Apk is generated in: app/build/outputs/apk/debug/app-debug.apk

## Uploading to Fabric Beta

you can use fastlane to automatically build and upload apk to fabric beta, use this command below :
```
    fastlane beta
```
don't forget to create directory and upload mapping to [google drive](https://drive.google.com/drive/folders/165ggAzFTJtJqK61ucz6JWxynJzsbq_eE),
directory name should be x.x.x/x.x.x.x (ex: 2.3.5/2.3.5.1 -> candidate for 2.3.5 playstore release, and beta build 2.3.5.1). 
copy apk and mapping directory from app/build/outputs for the live one

note : For installing fastline look at fastlane/README.md



## Git flow and pull requests

1. **develop** branch is the main branch! 
    The branch **master** is only used for Play store releases.
2. All commits should be inside a pull-request, allowing to:
    1. run CI scripts, asserting than you're not breaking build or tests.
    2. [share knowledge across team](https://www.leighhalliday.com/pull-requests-quality-training-shared-knowledge).

Gitlab pull requests are called **Merge Requests**.

After pushing your changes in a new branch in this repository or your fork,
open a Merge Request with **your new branch as source** and **zla/croissant:develop** as target,
and **main reviewer as Assignee** (someone else when team > 1 member).

## Create a new Release

Follow [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/#release) strategy:

1. Create **release/x.x.x** branch following [Semantic Versioning](https://semver.org).
2. Update **gradle.properties** *version_code* and *version_name*.
3. Update **CHANGELOG.md** with the new version as new first section.
4. Create a new directory on [google drive](https://drive.google.com/drive/folders/165ggAzFTJtJqK61ucz6JWxynJzsbq_eE),
    directory name should be x.x.x/playstore (ex: 2.3.5/playstore). copy live apk and mapping directory from app/build/outputs
    
note :
please note the team and Tanvir if you decide to change major version
    

## Update or Add Library

After you update or add some library please check do the following things :

- check the required proguard rules
- run release/live version and try to run some function that uses that library

## Coding conventions

By priority:

- [Android Kotlin Style Guide](https://android.github.io/kotlin-guides/style.html):
First source of truth for Android friendly Kotlin code.
- [Android Kotlin Interop Guide](https://android.github.io/kotlin-guides/interop.html):
even if all code should be written in Kotlin,
by following these rules we'll add an extra layer of code quality.
- [Kotlin Coding Conventions](https://kotlinlang.org/docs/reference/coding-conventions.html)

### Comments

```kotlin
// Single line comment with first letter uppercased and finishing with a dot.

// Also rules for
// multi-line comments.

/** Returns a one-line kdoc. Don't use @return for these ones. */

/**
 * Begins with a verb in order to quickly describe what's function purpose.
 *
 * Kotlin kdoc are markdown based, so no excuse to not care about javadoc rendering as well.
 *
 * See http://my-link
 * @see Class.function
 * @return something
 */
```

### Error handling

In order to provide user support as well as keeping track of all errors, 
you'll use [Crashlytics](https://fabric.io/primo7/android/apps/io.zla.app/issues?time=last-seven-days&event_type=all&subFilter=device&state=open&build%5B0%5D=top-builds)
with All Events / Crashes / Non Fatals:

- Crashes: when the app crashed. 
We should avoid it except if we're facing an unrecoverable state. 
We should avoid common issues, not planning to escape every impossible cases,
while putting **TODO** every time we're postponing a feature/fix/clean.

- Non Fatals: when we can recover an strange error, we should keep track of this event 
via Crashlytics with [Crashlytics.logException](https://docs.fabric.io/android/crashlytics/caught-exceptions.html?caught%20exceptions#caught-exceptions).
It should be considered as a crash that we successfully recovered.
A **TODO** should commonly be seen near this error handling.

For Crashlytics/Firebase, all sessions are sharing a **device_id**, allowing to link crashes with other services events, emails, etc.
A support request from the app should include this **device_id** or a way to resolve it.

### Debugging OOM
- Using Adroid Studio Profiling Tools
We can use android studio profiling tools to inspect memory usage when we use app, 
any peak that are not going down after we leaving certain screen can be considered some problem that need to be check up
- Using [Leak Canary](https://square.github.io/leakcanary/getting_started/)
Leak canary work by dumping java heap (.hprof) and parses it to show leak trace, after adding leak canary to the app just casually use the app,
visit some screen or action that you suspect to leak, when leak is found it will show you the screen with leak trace on it



## Knowledge repository

About production code:

- [RxJava](https://github.com/ReactiveX/RxJava) is used for asynchronous code.
- [Dagger](https://github.com/google/dagger) for dependency injection,
and the [Android support](https://google.github.io/dagger/android) from this library.
By doing so, we can manage global, Activity specific and Fragment/View specific objects lifecycle.
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html),
[ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel.html)
for a MVVM based view layer and
[LiveData](https://developer.android.com/topic/libraries/architecture/livedata.html)
for lifecycle aware asynchronous code.

About tests:

- [Mockito](http://site.mockito.org/)
- [AssertJ](http://joel-costigliola.github.io/assertj/) in order to write readable assertions.
- [Robolectric](http://robolectric.org/) can be used for Android specific unit tests.
**But** always try to abstract Android dependencies before using it:
unit tests are for dependency agnostic tests,
integration tests for testing code integration with Android,
and Robolectic is **not** Android!
And reduce [end-to-end tests](https://abstracta.us/blog/test-automation/best-testing-practices-for-agile-teams-the-automation-pyramid/) for your sake.
- [Espresso](https://developer.android.com/training/testing/espresso/index.html): TODO for integration tests.

## Tips

### Invalid JRE

If Java9 is installed, you'll see this weird error:

```bash
$ ./gradlew :app:assembleDebug
FAILURE: Build failed with an exception.

* What went wrong:
A problem occurred configuring project ':app'.
> Failed to notify project evaluation listener.
   > Could not initialize class com.android.sdklib.repository.AndroidSdkHandler
```

Create **~/.gradle/gradle.properties** with this content:

```properties
# Android Studio internal JRE
org.gradle.java.home=/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home
```

For command line tools like *uiautomatorviewer*, you have to manually define your JAVA_HOME:

```
# Install Java8
brew update
brew cask install caskroom/versions/java8
# Print current Java versions
/usr/libexec/java_home -V
# Override JAVA_HOME (put in .bashrc or .zshrc)
export JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"
```
